<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/aru.jpg"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    หน้าหลัก Research
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
</head>

<?php
$conn=new mysqli("localhost","root","porchaiz","research");
 if ($conn->connect_error){

   die("Connection failed: ".$conn->connect_erorr);
 }
 $sqls = "SELECT *,count(*) AS pe FROM personnel WHERE personnel.Personnel_id";
 $per_resulte = mysqli_query($conn,$sqls);
 $row1 = $per_resulte->fetch_assoc();
 
 $sqlr = "SELECT *,count(*) AS re FROM researches,yearbudget WHERE researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id='1'";
 $re_resulte = mysqli_query($conn,$sqlr);
 $row2 = $re_resulte->fetch_assoc();

 $sqlp = "SELECT *,count(*) AS pu FROM paper,yearpublication WHERE paper.yearpublication_id = yearpublication.yearpublication_id AND yearpublication.yearpublication_id = '1'";
 $pu_resulte = mysqli_query($conn,$sqlp);
 $row3 = $pu_resulte->fetch_assoc();

 $sqlt = "SELECT *,count(*) AS te FROM personnel,subdivision,mainagency WHERE personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND mainagency.MainAgency_id = '1'";
 $te_resulte = mysqli_query($conn,$sqlt);
 $row4 = $te_resulte->fetch_assoc();

 $sqlso = "SELECT *,count(*) AS so FROM personnel,subdivision,mainagency WHERE personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND mainagency.MainAgency_id = '2'";
 $so_resulte = mysqli_query($conn,$sqlso);
 $row5 = $so_resulte->fetch_assoc();

 $sqlsc = "SELECT *,count(*) AS sci FROM personnel,subdivision,mainagency WHERE personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND mainagency.MainAgency_id = '3'";
 $sc_resulte = mysqli_query($conn,$sqlsc);
 $row6 = $sc_resulte->fetch_assoc();

 $sqlm = "SELECT *,count(*) AS ma FROM personnel,subdivision,mainagency WHERE personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND mainagency.MainAgency_id = '4'";
 $ma_resulte = mysqli_query($conn,$sqlm);
 $row7 = $ma_resulte->fetch_assoc();

 $sqlb1 = "SELECT *,SUM(Budget) AS bug1 FROM doresearch,researches,personnel,subdivision,mainagency WHERE personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND mainagency.MainAgency_id = '1'";
 $b1_resulte = mysqli_query($conn,$sqlb1);
 $row8 = $b1_resulte->fetch_assoc();

 $sqlb2 = "SELECT *,SUM(Budget) AS bug2 FROM doresearch,researches,personnel,subdivision,mainagency WHERE personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND mainagency.MainAgency_id = '2'";
 $b2_resulte = mysqli_query($conn,$sqlb2);
 $row9 = $b2_resulte->fetch_assoc();

 $sqlb3 = "SELECT *,SUM(Budget) AS bug3 FROM doresearch,researches,personnel,subdivision,mainagency WHERE personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND mainagency.MainAgency_id = '3'";
 $b3_resulte = mysqli_query($conn,$sqlb3);
 $row10 = $b3_resulte->fetch_assoc();

 $sqlb4 = "SELECT *,SUM(Budget) AS bug4 FROM doresearch,researches,personnel,subdivision,mainagency WHERE personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND mainagency.MainAgency_id = '4'";
 $b4_resulte = mysqli_query($conn,$sqlb4);
 $row11 = $b4_resulte->fetch_assoc();


?>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo"><a href="https://www.aru.ac.th/rdi/" class="simple-text logo-normal">
    <img src="../assets/img/arun.jpg" alt="Girl in a jacket" width="120" height="145">
    <br style="font-size:18px;">สถาบันวิจัยและพัฒนา</br>  </a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active ">
            <a class="nav-link" href="./dashboardExee.php">
              <i class="material-icons">home</i>
              <p style="font-size:18px;">หน้าหลัก</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./userExee.php">
              <i class="material-icons">person</i>
              <p style="font-size:18px;">ข้อมูลส่วนตัว</p>
            </a>
          </li>
          <div class="logo"></div> 
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">ข้อมูลของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="researcherExee.php">ข้อมูลนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="tablesExee.php">ข้อมูลงานวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="publicationExee.php">ข้อมูลการตีพิมพ์</a>
                </div>
              </li>
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">รายงานของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="remainagencyExee.php">รายงานวิจัยคณะ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="repersonnelExee.php">รายงานนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="rebudgetExee.php">รายงานปีงบประมาณของคณะ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="rebudgetExees.php">รายงานปีงบประมาณ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="regrouptypeExee.php">รายงานประเภทเผยแพร่</a>
                  <a style="font-size:18px;" class="dropdown-item" href="reresearcherExee.php">รายงานทุนวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="republicationExee.php">รายงานการตีพิมพ์</a>
                </div>
              </li>       
        </ul>
</div>
    
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;">หน้าหลัก</a>
            <a class="navbar-brand" href="javascript:;">ระบบฐานข้อมูลบริหารการจัดการงานวิจัย มหาวิทยาลัยราชภัฏพระนครศรีอยุธยา</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: purple">
                <i class="material-icons">person</i>
                <?php echo "ผู้ใช้"."  ".$_SESSION['Positionname'].$_SESSION['Pname']."&nbsp&nbsp&nbsp".$_SESSION['Lname'];?> 
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                 <a class="dropdown-item" href="./userExee.php">Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="dashboardmain.php">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
         <font size="5" >สรุปข้อมูลงานวิจัยประจำปี 2564</font></p>
          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">person</i>
                  </div>
                  <p class="card-category"  ><font size="5"><a href="researcherExee.php">จำนวนนักวิจัย</a></font></p>
                  <h3 class="card-title"><?=$row1['pe']?> คน</h3>
                    <small></small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-danger"></i>
                    <a href="javascript:;"></a>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_paste</i>
                  </div>
                  <p class="card-category"><font size="5"><a href="tablesSExee.php">จำนวนเรื่องวิจัย</a></font></p>
                  <h3 class="card-title"><?=$row2['re']?> เรื่อง</h3>
                    <small></small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-danger"></i>
                    <a href="javascript:;"></a>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_paste</i>
                  </div>
                  <p class="card-category"><font size="5"><a href="publicationSExee.php">จำนวนการตีพิมพ์</a></font></p>
                  <h3 class="card-title"><?=$row3['pu']?> เรื่อง</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6 col-md-5 col-sm-5">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">person</i>
                  </div>
                  <p class="card-category"><font size="5">นักวิจัย 4 คณะ</font></p>
                  <script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js" ></script>
<script src="https://www.chartjs.org/samples/latest/utils.js" ></script>
<canvas id="pieChart"></canvas>
<script>
var ctxP = document.getElementById("pieChart").getContext('2d');
var myPieChart = new Chart(ctxP, {
	type: 'pie',
	data: {
		labels: ["คณะครุศาสตร์", "คณะมนุษยศาสตร์และสังคมศาสตร์", "คณะวิทยาศาสตร์และเทคโนโลยี", "คณะวิทยาการจัดการ"],
		datasets: [{
			data: [<?=$row4['te']?>, <?=$row5['so']?>, <?=$row6['sci']?>, <?=$row7['ma']?>],
			backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
			hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
		}]
	},
	options: {
		responsive: true
	}
});
</script>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">&#3647;</i>
                  </div>
                  <p class="card-category"><font size="5">งบประมาณ 4 คณะ</font></p>
                  <script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js" ></script>
<script src="https://www.chartjs.org/samples/latest/utils.js" ></script>
<canvas id="myCharts" style="max-width: 750px;"></canvas>
<script>
var ctx = document.getElementById("myCharts").getContext('2d');
var myCharts = new Chart(ctx, {
	type: 'bar',
	data: {
		labels: ["คณะวิทยาศาสตร์และเทคโนโลยี", "คณะครุศาสตร์", "คณะมนุษย์ศาสตร์และสังคมศาตร์", "คณะวิทยาการจัดการ"],
		datasets: [{
			label: 'งบประมาณ',
			data: [<?=$row10['bug3']?>, <?=$row8['bug1']?>, <?=$row9['bug2']?>, <?=$row11['bug4']?>],
			backgroundColor: [
				'rgba(255, 99, 132, 0.2)',
				'rgba(54, 162, 235, 0.2)',
				'rgba(255, 206, 86, 0.2)',
				'rgba(75, 192, 192, 0.2)',
				'rgba(153, 102, 255, 0.2)',
				'rgba(255, 159, 64, 0.2)'
			],
			borderColor: [
				'rgba(255,99,132,1)',
				'rgba(54, 162, 235, 1)',
				'rgba(255, 206, 86, 1)',
				'rgba(75, 192, 192, 1)',
				'rgba(153, 102, 255, 1)',
				'rgba(255, 159, 64, 1)'
			],
			borderWidth: 1
		}]
	},
	options: {
		scales: {
		yAxes: [{
			ticks: {
				beginAtZero: true
			}
		}]
		}
	}
});
</script>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>
            </div>

           
          <div class=" col-sm-15">
            <div class="card card-stats">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">campaign</i>
                </div>
                <p class="card-category"><font size="5">ข่าวประชาสัมพันธ์</font></p>
                <a class="nav-link">
                  <br><p></p></br>
                </a>
                <a class="nav-link" href="https://www.aru.ac.th/rdi/?page=download&subpage=detail&&id3=97">
                  <p>1. ประกาศมหาวิทยาลัยราชภัฏพระนครศรีอยุธยา เรื่อง ทุนสนับสนุนการพัฒนาข้อเสนอโครงการวิจัย หรือชุดโครงการวิจัย จากกองทุนวิจัยและการรับข้อเสนอโครงการวิจัยจากกองทุนวิจัย มหาวิทยาลัยราชภัฏพระนครศรีอยุธยา ประจำปีงบประมาณ พ.ศ. ๒๕๖๔ ครั้งที่ ๒</p>
                </a>
                <a class="nav-link" href="https://www.aru.ac.th/rdi/?page=download&subpage=detail&&id3=97">
                  <p>2. ประกาศมหาวิทยาลัยราชภัฏพระนครศรีอยุธยา เรื่อง ทุนสนับสนุนการพัฒนาข้อเสนอโครงการวิจัย หรือชุดโครงการวิจัย จากกองทุนวิจัยและการรับข้อเสนอโครงการวิจัยจากกองทุนวิจัย มหาวิทยาลัยราชภัฏพระนครศรีอยุธยา ประจำปีงบประมาณ พ.ศ. ๒๕๖๔ ครั้งที่ ๒</p>
                </a>
                <h3 class="card-title" href="#"></h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons"></i> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
     .row{
       margin-top: 20px;
        }

    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

    });
  </script>
</body>

</html>