<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/aru.jpg">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    ข้อมูลงานวิจัย
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />

  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
      background-color: #D6EAF8;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #D6EAF8;
      color: black;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
<?php
$conn=new mysqli("localhost","root","porchaiz","research");
 if ($conn->connect_error){

   die("Connection failed: ".$conn->connect_erorr);

 }
 $sql = "SELECT * FROM doresearch,researches,personnel,subdivision,mainagency,sourceoffunds,yearbudget,position WHERE personnel.Position_id = position.Position_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND personnel.SubDivision_id=subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND researches.SourceOfFund_id = sourceoffunds.SourceOfFund_id ORDER BY `doresearch`.`Research_id` DESC";
 $per_result = mysqli_query($conn,$sql);

 $sqls = "SELECT *,count(*) AS pe FROM personnel WHERE personnel.Personnel_id";
 $per_resulte = mysqli_query($conn,$sqls);
 $row1 = $per_resulte->fetch_assoc();

 $sqlr = "SELECT *,count(*) AS re FROM researches WHERE researches.Research_id";
 $re_resulte = mysqli_query($conn,$sqlr);
 $row2 = $re_resulte->fetch_assoc();


 $sqlb = "SELECT *,SUM(Budget) AS bug FROM doresearch,researches,personnel,subdivision,mainagency WHERE personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id";
 $b_resulte = mysqli_query($conn,$sqlb);
 $row3 = $b_resulte->fetch_assoc();

 $sqls = "SELECT * FROM mainagency order by MainAgency_id";
 $fun_resulte = mysqli_query($conn,$sqls);



 $sqly = "SELECT * FROM yearbudget order by yearbudget_id";
 $y_resulte = mysqli_query($conn,$sqly);
 
 $sqlsss = "SELECT * FROM sourceoffunds order by SourceOfFund_id";
 $so_resulte = mysqli_query($conn,$sqlsss);
 

?>

</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo"><a href="https://www.aru.ac.th/rdi/" class="simple-text logo-normal">
    <img src="../assets/img/arun.jpg" alt="Girl in a jacket" width="120" height="145">
    <br style="font-size:18px;">สถาบันวิจัยและพัฒนา</br> </a></div> 
    <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="./dashboard.php">
              <i class="material-icons">home</i>
              <p style="font-size:18px;">หน้าหลัก</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./user.php">
              <i class="material-icons">person</i>
              <p style="font-size:18px;">ข้อมูลส่วนตัว</p>
            </a>
          </li>
          <div class="logo"></div>
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">ตั้งค่าระบบงาน</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="uppersonnel.php">เพิ่มข้อมูลนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="upfund.php">เพิ่มข้อมูลแหล่งทุน</a>
                  <a style="font-size:18px;" class="dropdown-item" href="upresearch.php">เพิ่มข้อมูลงานวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="uppublishingsource.php">เพิ่มข้อมูลแหล่งการตีพิมพ์</a>
                  <a style="font-size:18px;" class="dropdown-item" href="uppublication.php">เพิ่มข้อมูลการตีพิมพ์</a>
                </div>
              </li> 
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">ข้อมูลของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="researcher.php">ข้อมูลนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="tables.php">ข้อมูลงานวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="publication.php">ข้อมูลการตีพิมพ์</a>
                </div>
              </li>
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">รายงานของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="remainagency.php">รายงานวิจัยคณะ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="repersonnel.php">รายงานนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="rebudget.php">รายงานปีงบประมาณ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="regrouptype.php">รายงานประเภทเผยแพร่</a>
                  <a style="font-size:18px;" class="dropdown-item" href="reresearcher.php">รายงานทุนวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="republication.php">รายงานการตีพิมพ์</a>
                </div>
              </li>       
        </ul>
    </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;" style="font-size:18px;">ข้อมูลงานวิจัย</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: purple">
                <i class="material-icons">person</i>
                <?php echo "ผู้ใช้"."  ".$_SESSION['Positionname'].$_SESSION['Pname']."&nbsp&nbsp&nbsp".$_SESSION['Lname'];?> 
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                 <a class="dropdown-item" href="./user.php">Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="dashboardmain.php">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            
          

         



            <div class="content">
        <div class="container-fluid">
          <div class="row">
          
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">person</i>
                  </div>
                  <p class="card-category"><font size="5">นักวิจัย</font></p>
                  <h3 class="card-title"><?=$row1['pe']?> คน</h3>
                    <small></small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-danger"></i>
                    <a href="javascript:;"></a>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_paste</i>
                  </div>
                  <p class="card-category"><font size="5">เรื่องวิจัย</font></p>
                  <h3 class="card-title"><?=$row2['re']?> เรื่อง</h3>
                    <small></small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-danger"></i>
                    <a href="javascript:;"></a>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">&#3647;</i>
                  </div>
                  <p class="card-category"><font size="5">งบประมาณทั้งหมด</font></p>
                  <h3 class="card-title"><?php echo number_format($row3['bug'])?> บาท</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>
          
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">งานวิจัย</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive" align="center" >
                    <table id="dtBasicExample"  class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                      <thead class=" text-primary" align="center">
                        <tr>
                        <th style="font-size:18px;" width="5%">ลำดับที่</th>
                        <th style="font-size:18px;" width="2%">คณะ</th>
                        <th style="font-size:18px;" width="15%">ชื่อนักวิจัย</th>
                        <th style="font-size:18px;" width="15%">โครงการวิจัย</th>
                        <th style="font-size:18px;" width="5%">ปีงบประมาณ</th>
                        <th style="font-size:18px;" width="13%">แหล่งทุน</th>
                        <th style="font-size:18px;" width="10%">งบประมาณ</th>
                        <th style="font-size:18px;" width="10%">วันที่เริ่มสัญญา</th>
                        <th style="font-size:18px;" width="10%">วันที่สิ้นสุดสัญญา</th>
                        <th style="font-size:18px;" width="2%">แก้ไข</th>
                        </tr>
                      </thead>
                      
                      <tbody>
                      <?php
                          while($row = $per_result->fetch_assoc()){

                            ?>
                           <tr>
                            <td style="font-size:18px;" align="center"><a href="detailresearch.php?Research_id=<?= $row['Research_id'];?>"><?= $row['Research_id']?></td>
                            <td style="font-size:18px;" align="left"><?= $row['NameMaSh']?></td>
                            <td style="font-size:18px;" align="left"><?= $row['Positionname'].$row['Pname']."&nbsp&nbsp&nbsp".$row['Lname']?></td>
                            <td style="font-size:18px;" align="left"><a href="detailresearch.php?Research_id=<?= $row['Research_id'];?>"><?= $row['ProjectR']?></td>
                            <td style="font-size:18px;" align="center"><?= $row['YearB']?></td>
                            <td style="font-size:18px;" align="left"><?= $row['NameFu']?></td>
                            <td style="font-size:18px;" align="right"><?php echo number_format($row['Budget']);?></td>
                            <td style="font-size:18px;" align="center"><?php echo DateThai($row['DayCon']);?></td>
                            <td style="font-size:18px;" align="center"><?php echo DateThai($row['EndCon']);?></td>
                            <td align="center"><a href="research_updpdate_form.php?Research_id=<?= $row['Research_id'];?>">
                                <img src="../assets/img/edit.png" width="30" height="30"></a></td>
                          </tr>
                          <?php } ?>
                      </tbody>   
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
    
      <!-- End Navbar -->
      
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
     function filterTable() {
  // Variables
  let dropdown, table, rows, cells, country, filter;
  dropdown = document.getElementById("countriesDropdown");
  table = document.getElementById("dtBasicExample");
  rows = table.getElementsByTagName("tr");
  filter = dropdown.value;

  // Loops through rows and hides those with countries that don't match the filter
  for (let row of rows) { // `for...of` loops through the NodeList
    cells = row.getElementsByTagName("td");
    country = cells[1] || null; // gets the 2nd `td` or nothing
    // if the filter is set to 'All', or this is the header row, or 2nd `td` text matches filter
    if (filter === "ทั้งหมด" || !country || (filter === country.textContent)) {
      row.style.display = ""; // shows this row
    }
    else {
      row.style.display = "none"; // hides this row
    }
  }
}
function filterTable2() {
  // Variables
  let dropdown, table, rows, cells, country, filter;
  dropdown = document.getElementById("countriesDropdown2");
  table = document.getElementById("dtBasicExample");
  rows = table.getElementsByTagName("tr");
  filter = dropdown.value;

  // Loops through rows and hides those with countries that don't match the filter
  for (let row of rows) { // `for...of` loops through the NodeList
    cells = row.getElementsByTagName("td");
    country = cells[4] || null; // gets the 2nd `td` or nothing
    // if the filter is set to 'All', or this is the header row, or 2nd `td` text matches filter
    if (filter === "ทั้งหมด" || !country || (filter === country.textContent)) {
      row.style.display = ""; // shows this row
    }
    else {
      row.style.display = "none"; // hides this row
    }
  }
}

function filterTable3() {
  // Variables
  let dropdown, table, rows, cells, country, filter;
  dropdown = document.getElementById("countriesDropdown3");
  table = document.getElementById("dtBasicExample");
  rows = table.getElementsByTagName("tr");
  filter = dropdown.value;

  // Loops through rows and hides those with countries that don't match the filter
  for (let row of rows) { // `for...of` loops through the NodeList
    cells = row.getElementsByTagName("td");
    country = cells[5] || null; // gets the 2nd `td` or nothing
    // if the filter is set to 'All', or this is the header row, or 2nd `td` text matches filter
    if (filter === "ทั้งหมด" || !country || (filter === country.textContent)) {
      row.style.display = ""; // shows this row
    }
    else {
      row.style.display = "none"; // hides this row
    }
  }
}
  </script>
  

  <script>
    $(document).ready(function() {


 
      



        $('#dtBasicExample').DataTable();
        $('.dataTables_length').addClass('bs-select');


      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
</body>
<?php
  function DateThai($strDate)
  {
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤษจิกายน","ธันวาคม");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
  }
  $strDate = "2008-08-14 13:42:44";
  // echo "ThaiCreate.Com Time now : ".DateThai($strDate);
?>
</html>