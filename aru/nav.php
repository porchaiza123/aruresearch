<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/aru.jpg">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    แก้ไขข้อมูลนักวิจัย
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
</head>

<div class="logo"><a href="https://www.aru.ac.th/rdi/" class="simple-text logo-normal">
      <img src="../assets/img/arun.jpg" alt="Girl in a jacket" width="120" height="145">
      <br style="font-size:18px;">สถาบันวิจัยและพัฒนา</br> </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active">
            <a class="nav-link" href="./dashboard.php">
              <i class="material-icons">home</i>
              <p style="font-size:18px;">หน้าหลัก</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./user.php">
              <i class="material-icons">person</i>
              <p style="font-size:18px;">ข้อมูลส่วนตัว</p>
            </a>
          </li>
          <div class="logo"></div>
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">ตั้งค่าระบบงาน</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="uppersonnel.php">เพิ่มข้อมูลนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="upfund.php">เพิ่มข้อมูลแหล่งทุน</a>
                  <a style="font-size:18px;" class="dropdown-item" href="upresearch.php">เพิ่มข้อมูลงานวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="uppublishingsource.php">เพิ่มข้อมูลแหล่งการตีพิมพ์</a>
                  <a style="font-size:18px;" class="dropdown-item" href="uppublication.php">เพิ่มข้อมูลการตีพิมพ์</a>
                </div>
              </li> 
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">ข้อมูลของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="researcher.php">ข้อมูลนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="tables.php">ข้อมูลงานวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="publication.php">ข้อมูลการตีพิมพ์</a>
                </div>
              </li>
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">รายงานของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="remainagency.php">รายงานวิจัยคณะ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="repersonnel.php">รายงานนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="rebudget.php">รายงานปีงบประมาณ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="regrouptype.php">รายงานประเภทเผยแพร่</a>
                  <a style="font-size:18px;" class="dropdown-item" href="reresearcher.php">รายงานทุนวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="republication.php">รายงานการตีพิมพ์</a>
                </div>
              </li>       
        </ul>
    </div>
    </div>