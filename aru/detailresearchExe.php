<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/aru.jpg">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    รายละเอียดงานวิจัย
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
</head>

<body class="">
    <style>
        .dropdown {
          position: relative;
          display: inline-block;
        }
        .dropdown-content {
          display: none;
          position: absolute;
          background-color: #f9f9f9;
          min-width: 160px;
          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
          padding: 12px 16px;
          z-index: 1;
        }
        .dropdown:hover .dropdown-content {
          display: block;
        }

        </style>
        <?php

$conn=new mysqli("localhost","root","porchaiz","research");
 if ($conn->connect_error){

   die("Connection failed: ".$conn->connect_erorr);

 }


 /*$sqle = "SELECT * FROM mainagency order by MainAgency_id";
 $main_resulte = mysqli_query($conn,$sqle);*/
 $id = $_GET['Research_id'];
 $sqlx = "select * from researches where Research_id='$id'";
 $result = mysqli_query($conn,$sqlx);
 $row = $result->fetch_assoc();
 $pub = $row['Publishingsource_id'];
 $sof = $row['SourceOfFund_id']; 

$sqlsm = "SELECT * FROM publishingsource where Publishingsource_id='$pub' ";
$pub_resultem = mysqli_query($conn,$sqlsm);

$sqlsz = "SELECT * FROM sourceoffunds where SourceOfFund_id='$sof' ";
$sof_resultem = mysqli_query($conn,$sqlsz);
 

$sqla = "SELECT * FROM researches,doresearch,personnel,sourceoffunds,yearbudget WHERE researches.yearbudget_id = yearbudget.yearbudget_id AND doresearch.Research_id = researches.Research_id AND researches.SourceOfFund_id = sourceoffunds.SourceOfFund_id And doresearch.Personnel_id = personnel.Personnel_id AND researches.Research_id='$id'";
$per_result = mysqli_query($conn,$sqla);
$row3 = $per_result->fetch_assoc();

 $sql = "SELECT * FROM doresearch,researches,personnel,sourceoffunds,publication,paper,grouptype,publishingsource,releasetype,yearpublication WHERE doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND publication.Research_id = researches.Research_id AND publication.paper_id = paper.paper_id AND publication.Publishingsource_id = publishingsource.Publishingsource_id AND researches.SourceOfFund_id = sourceoffunds.SourceOfFund_id AND publishingsource.grouptype_id = grouptype.grouptype_id AND paper.Research_id = researches.Research_id AND grouptype.Releasetype_id = releasetype.Releasetype_id AND paper.yearpublication_id = yearpublication.yearpublication_id AND publication.Research_id='$id'";
 $perx_result = mysqli_query($conn,$sql);






?>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo"><a href="https://www.aru.ac.th/rdi/" class="simple-text logo-normal">
    <img src="../assets/img/arun.jpg" alt="Girl in a jacket" width="120" height="145">
    <br style="font-size:18px;">สถาบันวิจัยและพัฒนา</br></a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="./dashboardExe.php">
              <i class="material-icons">home</i>
              <p style="font-size:18px;">หน้าหลัก</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./userExe.php">
              <i class="material-icons">person</i>
              <p style="font-size:18px;">ข้อมูลส่วนตัว</p>
            </a>
          </li>
          <div class="logo"></div> 
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">ข้อมูลของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="researcherExe.php">ข้อมูลนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="tablesExe.php">ข้อมูลงานวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="publicationExe.php">ข้อมูลการตีพิมพ์</a>
                </div>
              </li>
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">รายงานของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="remainagencyExe.php">รายงานวิจัยคณะ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="repersonnelExe.php">รายงานนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="rebudgetExe.php">รายงานปีงบประมาณ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="regrouptypeExe.php">รายงานประเภทเผยแพร่</a>
                  <a style="font-size:18px;" class="dropdown-item" href="reresearcherExe.php">รายงานทุนวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="republicationExe.php">รายงานการตีพิมพ์</a>
                </div>
              </li>       
        </ul>
</div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;" style="font-size:18px;">เพิ่มงานวิจัย</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: purple">
                <i class="material-icons">person</i>
                <?php echo "ผู้ใช้"."  ".$_SESSION['Positionname'].$_SESSION['Pname']."&nbsp&nbsp&nbsp".$_SESSION['Lname'];?> 
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                 <a class="dropdown-item" href="./userExe.php">Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="dashboardmain.php">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">รายละเอียดงานวิจัย</h4>
                </div>
                <div class="card-body">
                  <form>
                  <div class="col-md-">
                  <div class="row">
                        <div class="col-md-2">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">ชื่อ-นามสกุล</label>
                          <label></label>
                          <input style="font-size:18px;" style="width:1000px;" id ="Personnel_id" name = "Personnel_id" type="text" class="form-control" value="<?=$row3['Pname']."&nbsp&nbsp&nbsp".$row3['Lname'] ?>"readonly>
                        </div>
                      </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">โครงการวิจัย</label>
                          <label></label>
                          <input style="font-size:18px;" style="width:1000px;" id ="ProjectR" name = "ProjectR" type="text" class="form-control" value="<?= $row3['ProjectR'];?>"readonly>
                        </div>
                      </div>
                        </div>
                        <div class="row">
                        <div class="col-md-5">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">ปีงบประมาณ</label>
                          <label></label>
                          <input style="font-size:18px;" id ="YearB" name = "YearB" type="text" class="form-control" value="<?= $row3['YearB'];?>"readonly>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">งบประมาณ</label>
                          <label></label>
                          <input  style="font-size:18px;" id ="Budget" name = "Budget" type="text" class="form-control"value="<?= $row3['Budget'];?>"readonly>
                        </div>
                      </div>
                        </div>
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">วันที่เริ่มสัญญา</label>
                          <div class="col-md-6">
                           <div class="form-group">
                           <input  style="font-size:18px;"id ="DayCon" name = "DayCon" type="date" class="form-control"value="<?= $row3['DayCon'];?>"readonly>
                           </div>
                           </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">วันที่สิ้นสุดสัญญา</label>
                          <div class="col-md-6">
                           <div class="form-group">
                           <input style="font-size:18px;" id ="EndCon" name = "EndCon" type="date" class="form-control"value="<?= $row3['EndCon'];?>"readonly>
                           </div>
                           </div>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">แหล่งทุน</label>
                          <label></label>
                          <input  style="font-size:18px;" id ="SourceOfFund_id" name = "SourceOfFund_id" type="text" class="form-control"value="<?= $row3['NameFu'];?>"readonly>
                        </div>
                      </div>
                    
                    </div>
<!--
                    <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">แหล่งการตีพิมพ์</label>
                          <label></label>
                          <input  style="font-size:18px;" style="width:1000px;" id ="Publishingsource_id" name = "Publishingsource_id" type="text" class="form-control"value="<?= $row2['SourceName'];?>"readonly>
                        </div>
                      </div>
                        </div>
                        <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">ประเภทเผยแพร่</label>
                          <label></label>
                          <input  style="font-size:18px;" style="width:1000px;" id ="Releasetype_id" name = "Releasetype_id" type="text" class="form-control"value="<?= $row2['NameReleaseType'];?>"readonly>
                        </div>
                      </div>
                        </div>
                        <div class="row">
                    <div class="col-md-1">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">ปีที่พิมพ์</label>
                          <label></label>
                          <input  style="font-size:18px;" style="width:1000px;" id ="YearPublishing" name = "YearPublishing" type="text" class="form-control"value="<?= $row2['YearPublishing'];?>"readonly>
                        </div>
                      </div>
                        </div> -->
                        

                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>       
          </div>
        </div>
        <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">บทความที่ตีพิมพ์</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive" align="center" >
                    <table class="table">
                      <thead class=" text-primary" align="center">
                        <th style="font-size:18px;" width="8%">ชื่อบทความ</th>
                        <th style="font-size:18px;" width="10%">ชื่อแหล่งการตีพิมพ์</th>
                        <th style="font-size:18px;" width="8%">ประเภทแหล่งตีพิมพ์</th>
                        <th style="font-size:18px;" width="5%">ระดับ</th>
                        <th style="font-size:18px;" width="2%">ปีที่ตีพิมพ์</th>
                        <th style="font-size:18px;" width="2%">ไฟล์งาน</th>
                      </thead>
                      
                      <tbody>
                      <?php
                          while($row = $perx_result->fetch_assoc()){

                            ?>
                           <tr>
                           <td style="font-size:18px;" align="left"><?= $row['namepaper']?></td>
                           <td style="font-size:18px;" align="left"><?= $row['SourceName']."&nbsp"."ปีที่"."&nbsp".$row['The_year']."&nbsp"."ครั้งที่"."&nbsp".$row['The_times']."&nbsp"."ฉบับที่"."&nbsp".$row['No']."&nbsp".$row['The_day']."&nbsp".$row['YearP']?></td>
                           <td style="font-size:18px;" align="left"><?= $row['NameReleaseType']?></td>
                           <td style="font-size:18px;" align="center"><?= $row['groupname']?></td>
                           <td style="font-size:18px;" align="center"><?= $row['YearP']?></td>    
                           <td><center><a href="../assets/myfile/<?php echo $row["filepdf"];?>"><img src="../assets/img/folder.png" width="30" height="30"></a></center></td>
                          </tr>
                          <?php } ?>
                      </tbody>   
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
  
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
</body>

</html>