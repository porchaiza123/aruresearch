<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/aru.jpg">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    รายงานปีงบประมาณ
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />

  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
      background-color: #D6EAF8;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #D6EAF8;
      color: black;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>

<?php
$conn=new mysqli("localhost","root","porchaiz","research");
 if ($conn->connect_error){

   die("Connection failed: ".$conn->connect_erorr);

 }
 $id = $_POST['MainAgency_id'];

 $sqly = "SELECT * FROM mainagency order by MainAgency_id";
 $y_resulte = mysqli_query($conn,$sqly);


 $sqlb1 = "SELECT *,COUNT(*) AS bug1 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='1' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '5'";
 $b1_resulte = mysqli_query($conn,$sqlb1);
 $row8 = $b1_resulte->fetch_assoc();

 $sqlb2 = "SELECT *,COUNT(*) AS bug2 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='1' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '4'";
 $b2_resulte = mysqli_query($conn,$sqlb2);
 $row9 = $b2_resulte->fetch_assoc();

 $sqlb3 = "SELECT *,COUNT(*) AS bug3 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='1' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '3'";
 $b3_resulte = mysqli_query($conn,$sqlb3);
 $row10 = $b3_resulte->fetch_assoc();

 $sqlb4 = "SELECT *,COUNT(*) AS bug4 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='1' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '2'";
 $b4_resulte = mysqli_query($conn,$sqlb4);
 $row11 = $b4_resulte->fetch_assoc();

 $sqlb5 = "SELECT *,COUNT(*) AS bug5 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='1' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '1'";
 $b5_resulte = mysqli_query($conn,$sqlb5);
 $row12 = $b5_resulte->fetch_assoc();



 $sqlb6 = "SELECT *,SUM(Budget) AS bug6 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='1' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '5'";
 $b6_resulte = mysqli_query($conn,$sqlb6);
 $row13 = $b6_resulte->fetch_assoc();

 $sqlb7 = "SELECT *,SUM(Budget) AS bug7 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='1' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '4'";
 $b7_resulte = mysqli_query($conn,$sqlb7);
 $row14 = $b7_resulte->fetch_assoc();

 $sqlb8 = "SELECT *,SUM(Budget) AS bug8 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='1' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '3'";
 $b8_resulte = mysqli_query($conn,$sqlb8);
 $row15 = $b8_resulte->fetch_assoc();

 $sqlb9 = "SELECT *,SUM(Budget) AS bug9 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='1' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '2'";
 $b9_resulte = mysqli_query($conn,$sqlb9);
 $row16 = $b9_resulte->fetch_assoc();

 $sqlb10 = "SELECT *,SUM(Budget) AS bug10 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='1' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '1'";
 $b10_resulte = mysqli_query($conn,$sqlb10);
 $row17 = $b10_resulte->fetch_assoc();


 
 $sqlb11 = "SELECT *,COUNT(*) AS bug11 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='2' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '5'";
 $b11_resulte = mysqli_query($conn,$sqlb11);
 $row18 = $b11_resulte->fetch_assoc();

 $sqlb12 = "SELECT *,COUNT(*) AS bug12 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='2' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '4'";
 $b12_resulte = mysqli_query($conn,$sqlb12);
 $row19 = $b12_resulte->fetch_assoc();

 $sqlb13 = "SELECT *,COUNT(*) AS bug13 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='2' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '3'";
 $b13_resulte = mysqli_query($conn,$sqlb13);
 $row20 = $b13_resulte->fetch_assoc();

 $sqlb14 = "SELECT *,COUNT(*) AS bug14 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='2' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '2'";
 $b14_resulte = mysqli_query($conn,$sqlb14);
 $row21 = $b14_resulte->fetch_assoc();

 $sqlb15 = "SELECT *,COUNT(*) AS bug15 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='2' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '1'";
 $b15_resulte = mysqli_query($conn,$sqlb15);
 $row22 = $b15_resulte->fetch_assoc();



 $sqlb16 = "SELECT *,SUM(Budget) AS bug16 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='2' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '5'";
 $b16_resulte = mysqli_query($conn,$sqlb16);
 $row23 = $b16_resulte->fetch_assoc();

 $sqlb17 = "SELECT *,SUM(Budget) AS bug17 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='2' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '4'";
 $b17_resulte = mysqli_query($conn,$sqlb17);
 $row24 = $b17_resulte->fetch_assoc();

 $sqlb18 = "SELECT *,SUM(Budget) AS bug18 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='2' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '3'";
 $b18_resulte = mysqli_query($conn,$sqlb18);
 $row25 = $b18_resulte->fetch_assoc();

 $sqlb19 = "SELECT *,SUM(Budget) AS bug19 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='2' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '2'";
 $b19_resulte = mysqli_query($conn,$sqlb19);
 $row26 = $b19_resulte->fetch_assoc();

 $sqlb20 = "SELECT *,SUM(Budget) AS bug20 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='2' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '1'";
 $b20_resulte = mysqli_query($conn,$sqlb20);
 $row27 = $b20_resulte->fetch_assoc();



 $sqlb21 = "SELECT *,COUNT(*) AS bug21 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='3' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '5'";
 $b21_resulte = mysqli_query($conn,$sqlb21);
 $row28 = $b21_resulte->fetch_assoc();

 $sqlb22 = "SELECT *,COUNT(*) AS bug22 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='3' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '4'";
 $b22_resulte = mysqli_query($conn,$sqlb22);
 $row29 = $b22_resulte->fetch_assoc();

 $sqlb23 = "SELECT *,COUNT(*) AS bug23 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='3' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '3'";
 $b23_resulte = mysqli_query($conn,$sqlb23);
 $row30 = $b23_resulte->fetch_assoc();

 $sqlb24 = "SELECT *,COUNT(*) AS bug24 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='3' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '2'";
 $b24_resulte = mysqli_query($conn,$sqlb24);
 $row31 = $b24_resulte->fetch_assoc();

 $sqlb25 = "SELECT *,COUNT(*) AS bug25 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='3' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '1'";
 $b25_resulte = mysqli_query($conn,$sqlb25);
 $row32 = $b25_resulte->fetch_assoc();


 $sqlb26 = "SELECT *,SUM(Budget) AS bug26 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='3' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '5'";
 $b26_resulte = mysqli_query($conn,$sqlb26);
 $row33 = $b26_resulte->fetch_assoc();

 $sqlb27 = "SELECT *,SUM(Budget) AS bug27 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='3' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '4'";
 $b27_resulte = mysqli_query($conn,$sqlb27);
 $row34 = $b27_resulte->fetch_assoc();

 $sqlb28 = "SELECT *,SUM(Budget) AS bug28 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='3' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '3'";
 $b28_resulte = mysqli_query($conn,$sqlb28);
 $row35 = $b28_resulte->fetch_assoc();

 $sqlb29 = "SELECT *,SUM(Budget) AS bug29 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='3' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '2'";
 $b29_resulte = mysqli_query($conn,$sqlb29);
 $row36 = $b29_resulte->fetch_assoc();

 $sqlb30 = "SELECT *,SUM(Budget) AS bug30 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='3' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '1'";
 $b30_resulte = mysqli_query($conn,$sqlb30);
 $row37 = $b30_resulte->fetch_assoc();


 $sqlb31 = "SELECT *,COUNT(*) AS bug31 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='4' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '5'";
 $b31_resulte = mysqli_query($conn,$sqlb31);
 $row38 = $b31_resulte->fetch_assoc();

 $sqlb32 = "SELECT *,COUNT(*) AS bug32 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='4' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '4'";
 $b32_resulte = mysqli_query($conn,$sqlb32);
 $row39 = $b32_resulte->fetch_assoc();

 $sqlb33 = "SELECT *,COUNT(*) AS bug33 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='4' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '3'";
 $b33_resulte = mysqli_query($conn,$sqlb33);
 $row40 = $b33_resulte->fetch_assoc();

 $sqlb34 = "SELECT *,COUNT(*) AS bug34 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='4' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '2'";
 $b34_resulte = mysqli_query($conn,$sqlb34);
 $row41 = $b34_resulte->fetch_assoc();

 $sqlb35 = "SELECT *,COUNT(*) AS bug35 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='4' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '1'";
 $b35_resulte = mysqli_query($conn,$sqlb35);
 $row42 = $b35_resulte->fetch_assoc();


 $sqlb36 = "SELECT *,SUM(Budget) AS bug36 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='4' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '5'";
 $b36_resulte = mysqli_query($conn,$sqlb36);
 $row43 = $b36_resulte->fetch_assoc();

 $sqlb37 = "SELECT *,SUM(Budget) AS bug37 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='4' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '4'";
 $b37_resulte = mysqli_query($conn,$sqlb37);
 $row44 = $b37_resulte->fetch_assoc();

 $sqlb38 = "SELECT *,SUM(Budget) AS bug38 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='4' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '3'";
 $b38_resulte = mysqli_query($conn,$sqlb38);
 $row45 = $b38_resulte->fetch_assoc();

 $sqlb39 = "SELECT *,SUM(Budget) AS bug39 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='4' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '2'";
 $b39_resulte = mysqli_query($conn,$sqlb39);
 $row46 = $b39_resulte->fetch_assoc();

 $sqlb40 = "SELECT *,SUM(Budget) AS bug40 FROM doresearch,researches,personnel,subdivision,mainagency,yearbudget WHERE mainagency.MainAgency_id='4' AND personnel.SubDivision_id = subdivision.SubDivision_id AND subdivision.MainAgency_id = mainagency.MainAgency_id AND doresearch.Research_id = researches.Research_id AND doresearch.Personnel_id = personnel.Personnel_id AND researches.yearbudget_id = yearbudget.yearbudget_id AND yearbudget.yearbudget_id = '1'";
 $b40_resulte = mysqli_query($conn,$sqlb40);
 $row47 = $b40_resulte->fetch_assoc();




?>

</head>


<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo"><a href="https://www.aru.ac.th/rdi/" class="simple-text logo-normal">
    <img src="../assets/img/arun.jpg" alt="Girl in a jacket" width="120" height="145">
    <br>สถาบันวิจัยและพัฒนา</br>  </a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="./dashboardExe.php">
              <i class="material-icons">home</i>
              <p style="font-size:18px;">หน้าหลัก</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./userExe.php">
              <i class="material-icons">person</i>
              <p style="font-size:18px;">ข้อมูลส่วนตัว</p>
            </a>
          </li>
          <div class="logo"></div> 
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">ข้อมูลของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="researcherExe.php">ข้อมูลนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="tablesExe.php">ข้อมูลงานวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="publicationExe.php">ข้อมูลการตีพิมพ์</a>
                </div>
              </li>
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">รายงานของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="remainagencyExe.php">รายงานวิจัยคณะ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="repersonnelExe.php">รายงานนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="rebudgetExe.php">รายงานปีงบประมาณ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="regrouptypeExe.php">รายงานประเภทเผยแพร่</a>
                  <a style="font-size:18px;" class="dropdown-item" href="reresearcherExe.php">รายงานทุนวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="republicationExe.php">รายงานการตีพิมพ์</a>
                </div>
              </li>       
        </ul>
</div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;">รายงานปีงบประมาณ</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: purple">
                <i class="material-icons">person</i>
                <?php echo "ผู้ใช้"."  ".$_SESSION['Positionname'].$_SESSION['Pname']."&nbsp&nbsp&nbsp".$_SESSION['Lname'];?> 
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                 <a class="dropdown-item" href="./userExe.php">Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="dashboardmain.php">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>     
      <div class="content">
        <div class="container-fluid">
          <div class="col-lg-12 col-md-6 col-sm-6">
            <div class="">
              
           

            <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_paste</i>
                  </div>
                  <p class="card-category"><font size="5">งานวิจัยประจำปีคณะครุศาสตร์</font></p>
                  <canvas id="myChart" width="400" height="200"></canvas>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
 
    <script>
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["ปี 2560", "ปี 2561", "ปี 2562", "ปี 2563", "ปี 2564"],
                datasets: [{
                    label: 'จำนวนวิจัย',
                    data:  [<?=$row8['bug1']?>, <?=$row9['bug2']?>, <?=$row10['bug3']?>, <?=$row11['bug4']?>, <?=$row12['bug5']?>],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
        </script> 
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">&#3647;</i>
                  </div>
                  <p class="card-category"><font size="5">งบประมาณประจำปีคณะครุศาสตร์</font></p>
                  
                  <script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js" ></script>
<script src="https://www.chartjs.org/samples/latest/utils.js" ></script>
<canvas id="pieChart"></canvas>
<script>
var ctxP = document.getElementById("pieChart").getContext('2d');
var myPieChart = new Chart(ctxP, {
	type: 'pie',
	data: {
		labels: ["ปี 2560", "ปี 2561", "ปี 2562", "ปี 2563", "ปี 2564"],
		datasets: [{
			data: [<?=$row13['bug6']?>, <?=$row14['bug7']?>, <?=$row15['bug8']?>, <?=$row16['bug9']?>, <?=$row17['bug10']?>],
			backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
			hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
		}]
    
	},
	options: {
		responsive: true
	}
});
</script>
                   
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_paste</i>
                  </div>
                  <p class="card-category"><font size="5">งานวิจัยประจำปีคณะมนุษยศาสตร์และสังคมศาสตร์</font></p>
                  <canvas id="myChart1" width="400" height="200"></canvas>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
 
    <script>
        var ctx = document.getElementById("myChart1");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["ปี 2560", "ปี 2561", "ปี 2562", "ปี 2563", "ปี 2564"],
                datasets: [{
                    label: 'จำนวนวิจัย',
                    data: [<?=$row18['bug11']?>, <?=$row19['bug12']?>, <?=$row20['bug13']?>, <?=$row21['bug14']?>, <?=$row22['bug15']?>],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
        </script> 
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">&#3647;</i>
                  </div>
                  <p class="card-category"><font size="5">งบประมาณประจำปีคณะมนุษยศาสตร์และสังคมศาสตร์</font></p>
                  
                  <script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js" ></script>
<script src="https://www.chartjs.org/samples/latest/utils.js" ></script>
<canvas id="pieChart1"></canvas>
<script>
var ctxP = document.getElementById("pieChart1").getContext('2d');
var myPieChart = new Chart(ctxP, {
	type: 'pie',
	data: {
		labels: ["ปี 2560", "ปี 2561", "ปี 2562", "ปี 2563", "ปี 2564"],
		datasets: [{
			data: [<?=$row23['bug16']?>, <?=$row24['bug17']?>, <?=$row25['bug18']?>, <?=$row26['bug19']?>, <?=$row27['bug20']?>],
			backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
			hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
		}]
	},
	options: {
		responsive: true
	}
});
</script>
                   
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>

          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_paste</i>
                  </div>
                  <p class="card-category"><font size="5">งานวิจัยประจำปีคณะวิทยาศาสตร์และเทคโนโลยี</font></p>
                  <canvas id="myChart2" width="400" height="200"></canvas>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
 
    <script>
        var ctx = document.getElementById("myChart2");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["ปี 2560", "ปี 2561", "ปี 2562", "ปี 2563", "ปี 2564"],
                datasets: [{
                    label: 'จำนวนวิจัย',
                    data: [<?=$row28['bug21']?>, <?=$row29['bug22']?>, <?=$row30['bug23']?>, <?=$row31['bug24']?>, <?=$row32['bug25']?>],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
        </script> 
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">&#3647;</i>
                  </div>
                  <p class="card-category"><font size="5">งบประมาณประจำปีคณะวิทยาศาสตร์และเทคโนโลยี</font></p>
                  
                  <script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js" ></script>
<script src="https://www.chartjs.org/samples/latest/utils.js" ></script>
<canvas id="pieChart2"></canvas>
<script>
var ctxP = document.getElementById("pieChart2").getContext('2d');
var myPieChart = new Chart(ctxP, {
	type: 'pie',
	data: {
		labels: ["ปี 2560", "ปี 2561", "ปี 2562", "ปี 2563", "ปี 2564"],
		datasets: [{
			data: [<?=$row33['bug26']?>, <?=$row34['bug27']?>, <?=$row35['bug28']?>, <?=$row36['bug29']?>, <?=$row37['bug30']?>],
			backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
			hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
		}]
	},
	options: {
		responsive: true
	}
});
</script>
                   
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>


          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_paste</i>
                  </div>
                  <p class="card-category"><font size="5">งานวิจัยประจำปีคณะวิทยาการจัดการ</font></p>
                  <canvas id="myChart3" width="400" height="200"></canvas>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
 
    <script>
        var ctx = document.getElementById("myChart3");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["ปี 2560", "ปี 2561", "ปี 2562", "ปี 2563", "ปี 2564"],
                datasets: [{
                    label: 'จำนวนวิจัย',
                    data: [<?=$row38['bug31']?>, <?=$row39['bug32']?>, <?=$row40['bug33']?>, <?=$row41['bug34']?>, <?=$row42['bug35']?>],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
        </script> 
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">&#3647;</i>
                  </div>
                  <p class="card-category"><font size="5">งบประมาณประจำปีคณะวิทยาการจัดการ</font></p>
                  
                  <script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js" ></script>
<script src="https://www.chartjs.org/samples/latest/utils.js" ></script>
<canvas id="pieChart3"></canvas>
<script>
var ctxP = document.getElementById("pieChart3").getContext('2d');
var myPieChart = new Chart(ctxP, {
	type: 'pie',
	data: {
		labels: ["ปี 2560", "ปี 2561", "ปี 2562", "ปี 2563", "ปี 2564"],
		datasets: [{
			data: [<?=$row43['bug36']?>, <?=$row44['bug37']?>, <?=$row45['bug38']?>, <?=$row46['bug39']?>, <?=$row47['bug40']?>],
			backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
			hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
		}]
	},
	options: {
		responsive: true
	}
});
</script>
                   
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>

          <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_paste</i>
                    
                  </div>
                  <p class="card-category"><font size="5">เปรียบเทียบงานวิจัยแต่ละคณะ</font></p>
                  
                  <canvas id="myChart5" width="400" height="200"></canvas>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
 
    <script>
        var ctx = document.getElementById("myChart5");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["ปี 2560", "ปี 2561", "ปี 2562", "ปี 2563", "ปี 2564"],
                datasets: [{ 
                label: 'คณะครุศาสตร์',
                data: [<?=$row8['bug1']?>, <?=$row9['bug2']?>, <?=$row10['bug3']?>, <?=$row11['bug4']?>, <?=$row12['bug5']?>],
                borderColor: "red",
                fill: false
                }, { 
                label: 'คณะมนุษยศาสตร์และสังคมศาสตร์',
                 data: [<?=$row18['bug11']?>, <?=$row19['bug12']?>, <?=$row20['bug13']?>, <?=$row21['bug14']?>, <?=$row22['bug15']?>],
                 borderColor: "green",
                fill: false
                }, { 
                label: 'คณะวิทยาศาสตร์และเทคโนโลยี',
                data: [<?=$row28['bug21']?>, <?=$row29['bug22']?>, <?=$row30['bug23']?>, <?=$row31['bug24']?>, <?=$row32['bug25']?>],
                borderColor: "blue",
                fill: false
                }, { 
                label: 'คณะวิทยาการจัดการ',
                data: [<?=$row38['bug31']?>, <?=$row39['bug32']?>, <?=$row40['bug33']?>, <?=$row41['bug34']?>, <?=$row42['bug35']?>],
                borderColor: "purple",
                fill: false
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
        </script> 

                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">&#3647;</i>
                  </div>
                  <p class="card-category"><font size="5">เปรียบเทียบงบประมาณแต่ละคณะ</font></p>
                  
                  <canvas id="myChart6" width="400" height="200"></canvas>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
 
    <script>
        var ctx = document.getElementById("myChart6");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["ปี 2560", "ปี 2561", "ปี 2562", "ปี 2563", "ปี 2564"],
                datasets: [{ 
                label: 'คณะครุศาสตร์',
                data: [<?=$row13['bug6']?>, <?=$row14['bug7']?>, <?=$row15['bug8']?>, <?=$row16['bug9']?>, <?=$row17['bug10']?>],
                borderColor: "red",
                fill: false
                }, { 
                label: 'คณะมนุษยศาสตร์และสังคมศาสตร์',
                 data: [<?=$row23['bug16']?>, <?=$row24['bug17']?>, <?=$row25['bug18']?>, <?=$row26['bug19']?>, <?=$row27['bug20']?>],
                 borderColor: "green",
                fill: false
                }, { 
                label: 'คณะวิทยาศาสตร์และเทคโนโลยี',
                data: [<?=$row33['bug26']?>, <?=$row34['bug27']?>, <?=$row35['bug28']?>, <?=$row36['bug29']?>, <?=$row37['bug30']?>],
                borderColor: "blue",
                fill: false
                }, { 
                label: 'คณะวิทยาการจัดการ',
                data: [<?=$row43['bug36']?>, <?=$row44['bug37']?>, <?=$row45['bug38']?>, <?=$row46['bug39']?>, <?=$row47['bug40']?>],
                borderColor: "purple",
                fill: false
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
        </script> 

                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons"></i> 
                  </div>
                </div>
              </div>
            </div>


            
            
          </div>

        </div>
      </div>
      </div>
      </div>
      </div>
      <!-- End Navbar -->
      
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
    $(document).ready(function() {


      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
</body>

</html>