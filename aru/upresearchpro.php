<!--
=========================================================
Material Dashboard - v2.1.2
=========================================================

Product Page: https://www.creative-tim.com/product/material-dashboard
Copyright 2020 Creative Tim (https://www.creative-tim.com)
Coded by Creative Tim

=========================================================
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/aru.jpg">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    เพิ่มงานวิจัย
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
</head>
<?php

$conn=new mysqli("localhost","root","porchaiz","research");
 if ($conn->connect_error){

   die("Connection failed: ".$conn->connect_erorr);

 }

 $sqls = "SELECT * FROM sourceoffunds order by SourceOfFund_id";
 $sou_resulte = mysqli_query($conn,$sqls);

 $sqla = "SELECT * FROM publishingsource order by Publishingsource_id";
 $pu_resulte = mysqli_query($conn,$sqla);

 $sqly = "SELECT * FROM yearbudget order by yearbudget_id";
 $yb_resulte = mysqli_query($conn,$sqly);

 $sql = "select * from personnel,subdivision,mainagency where personnel.SubDivision_id = subdivision.SubDivision_id and  subdivision.MainAgency_id = mainagency.MainAgency_id ";
 
 $per_result = mysqli_query($conn,$sql);
 

?>

<body class="">
    <style>
        .dropdown {
          position: relative;
          display: inline-block;
        }
        .dropdown-content {
          display: none;
          position: absolute;
          background-color: #f9f9f9;
          min-width: 160px;
          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
          padding: 12px 16px;
          z-index: 1;
        }
        .dropdown:hover .dropdown-content {
          display: block;
        }

        </style>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo"><a href="https://www.aru.ac.th/rdi/" class="simple-text logo-normal">
    <img src="../assets/img/arun.jpg" alt="Girl in a jacket" width="120" height="145">
    <br style="font-size:18px;">สถาบันวิจัยและพัฒนา</br>  </a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="./dashboardpro.php">
              <i class="material-icons">home</i>
              <p style="font-size:18px;">หน้าหลัก</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./userpro.php">
              <i class="material-icons">person</i>
              <p style="font-size:18px;">ข้อมูลส่วนตัว</p>
            </a>
          </li>
          <div class="logo"></div>
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">ตั้งค่าระบบงาน</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="upresearchpro.php">เพิ่มข้อมูลงานวิจัย</a>
                </div>
              </li> 
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">ข้อมูลของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="researcherpro.php">ข้อมูลนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="tablespro.php">ข้อมูลงานวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="publicationpro.php">ข้อมูลการตีพิมพ์</a>
                </div>
              </li>
          <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-caret-down"></i>
                <p style="font-size:18px;">รายงานของระบบ</p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a style="font-size:18px;" class="dropdown-item" href="remainagencypro.php">รายงานวิจัยคณะ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="repersonnelpro.php">รายงานนักวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="rebudgetpro.php">รายงานปีงบประมาณ</a>
                  <a style="font-size:18px;" class="dropdown-item" href="regrouptypepro.php">รายงานประเภทเผยแพร่</a>
                  <a style="font-size:18px;" class="dropdown-item" href="reresearcherpro.php">รายงานทุนวิจัย</a>
                  <a style="font-size:18px;" class="dropdown-item" href="republicationpro.php">รายงานการตีพิมพ์</a>
                </div>
              </li>       
        </ul>
</div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;" style="font-size:18px;">เพิ่มงานวิจัย</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: purple">
                <i class="material-icons">person</i>
                <?php echo "ผู้ใช้"."  ".$_SESSION['Positionname'].$_SESSION['Pname']."&nbsp&nbsp&nbsp".$_SESSION['Lname'];?> 
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                 <a class="dropdown-item" href="./userpro.php">Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="dashboardmain.php">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">เพิ่มงานวิจัย</h4>
                </div>
                <div class="card-body">
                  <form action="insertresearch.php" method="post">
                  <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                    <div class="form-group">
                            <label for="cars" style="font-size:18px;">ชื่อ-นามสกุล : </label>
                            <select style="width: 200px;" id ="Personnel_id" name = "Personnel_id">
                              <?php
                                 while($row = $per_result->fetch_assoc()){
                              ?>
                                  <option  value = "<?=$row['Personnel_id']?>"><?=$row['Pname']."&nbsp&nbsp&nbsp".$row['Lname'] ?></option>
                                     <?php } ?>
                               </select> 
                               <i class="fa fa-caret-down"></i>      
                        </div>
                        </div>
                        <div class="col-md-2">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">คณะ :</label>
                          <select style="width: 180px;" id ="MainAgency_id" name = "MainAgency_id"disabled>
                            <option   value = ""></option>
                               </select>
                        </div>
                      </div>
                      <div class="col-md-2.5">
                        <div  class="form-group">
                          <label style="font-size:18px;"  class="bmd-label-floating">ระดับปริญญา :</label>
                          <select style="width: 180px;" id ="degree_id" name = "degree_id" disabled>
                            <option   value = ""></option >
                               </select>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div  class="form-group">
                          <label style="font-size:18px;"  class="bmd-label-floating">สาขา :</label>
                          <select style="width: 175px;" id ="SubDivision_id" name = "SubDivision_id"disabled>
                            <option   value = ""></option>
                               </select>
                        </div>
                       </div>
                    </div>   
                    <div class="row">
                        <div class="col-md-10">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">ชื่อโครงการวิจัย</label>
                          <input style="font-size:18px;" style="width:1000px;" id ="ProjectR" name = "ProjectR" type="text" class="form-control" >
                        </div>
                      </div>
                        </div>
                        <div class="row">
                        <div class="col-md-4">
                       <div class="form-group">
                            <label style="font-size:18px;" for="cars">ปีงบประมาณ</label>
                            <select style="width:100px;" id ="yearbudget_id"  name ="yearbudget_id">
                              <?php
                                 while($row = $yb_resulte->fetch_assoc()){
                              ?>
                                  <option value = "<?=$row['yearbudget_id']?>"><?=$row['YearB']?></option>
                                     <?php } ?>
                               </select>
                               <i class="fa fa-caret-down"></i>
                         </div>
                        </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">งบประมาณ(บาท)</label>
                          <input  style="font-size:18px;" id ="Budget" name = "Budget" type="text" class="form-control">
                        </div>
                      </div>
                        </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                        <label style="font-size:18px;" class="bmd-label-floating">วันที่เริ่มสัญญา</label>
                          <div class="col-md-6">
                           <div class="form-group">
                           <input  style="font-size:18px;"id ="DayCon" name = "DayCon" type="date" class="form-control">
                           </div>
                           </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label style="font-size:18px;" class="bmd-label-floating">วันที่สิ้นสุดสัญญา</label>
                          <div class="col-md-6">
                           <div class="form-group">
                           <input style="font-size:18px;" id ="EndCon" name = "EndCon" type="date" class="form-control"  >
                           </div>
                           </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                       <div class="form-group">
                            <label style="font-size:18px;" for="cars">แหล่งทุน</label>
                            <select id ="SourceOfFund_id" name ="SourceOfFund_id">
                              <?php
                                 while($row = $sou_resulte->fetch_assoc()){
                              ?>
                                  <option value = "<?=$row['SourceOfFund_id']?>"><?=$row['NameFu']?></option>
                                     <?php } ?>
                               </select>
                               <i class="fa fa-caret-down"></i>
                         </div>
                        </div>
                        <!-- End Navbar 
                        <div class="col-md-12">
                       <div class="form-group">
                            <label style="font-size:18px;" for="cars">แหล่งการตีพิมพ์</label>
                            <select id ="Publishingsource_id" name ="Publishingsource_id">
                              <?php
                                 while($row = $pu_resulte->fetch_assoc()){
                              ?>
                                  <option value = "<?=$row['Publishingsource_id']?>"><?=$row['SourceName']?></option>
                                     <?php } ?>
                               </select>
                         </div>
                        </div>
                        </div>
                        <form>
                        <label style="font-size:18px;" for="cars">อัพโหลดรูปเล่ม: </label>
                          <input type="file" id="Upload" name="Upload">
                        </form> -->
                    
                    <div class="col-md-5">
                    <button type="submit" class="btn btn-primary pull-right">เพิ่ม</button>
                    </div>
                    
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
        
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
    $(document).ready(function() {

      $('#Personnel_id').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {Personnel_id: $(this).val()},
                        url: 'selectper_subdivision.php',
                        success: function(data) {
                            $('#SubDivision_id').html(data);
                        },
            error: function(jqXHR, text, error){
            // Displaying if there are any errors
                  $('#results').html(error);           
        }
                    });
                    return false;
                });
      $('#Personnel_id').change(function() {

                    $.ajax({
                        type: 'POST',
                        data: {Personnel_id: $(this).val()},
                        url: 'selectper_mainagency.php',
                        success: function(data) {
                            $('#MainAgency_id').html(data);
                        },
            error: function(jqXHR, text, error){
            // Displaying if there are any errors
                  $('#results').html(error);           
        }
                    });
                    return false;
                });

                $('#Personnel_id').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {Personnel_id: $(this).val()},
                        url: 'selectper_degree.php',
                        success: function(data) {
                            $('#degree_id').html(data);
                        },
            error: function(jqXHR, text, error){
            // Displaying if there are any errors
                  $('#results').html(error);           
        }
                    });
                    return false;
                });








      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
</body>

</html>